#!/bin/sh

echo "Deploying to prod..."

echo "Connecting to remote"
ssh -t -t root@rvasanda.com << 'ENDSSH'

echo "Kill all detached screens"

screen -ls | grep Detached | cut -d. -f1 | awk '{print $1}' | xargs kill

echo "Cleanup existing smart-shopper directory"

rm -rf smart-shopper

echo "Get the latest code"

# git clone https://rvasanda@bitbucket.org/rvasanda/smart-shopper.git -b smartshopper_release_1.0 --single-branch
git clone https://rvasanda:Escaflowne1@bitbucket.org/rvasanda/smart-shopper.git
cd smart-shopper
git checkout -- .
git pull
mkdir logs

echo "Install any missing dependencies..."

npm install

echo "Run the node server..."

screen -dm npm run prod

exit

exit

ENDSSH