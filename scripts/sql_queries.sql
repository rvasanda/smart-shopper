SELECT
    Products.productName,
    min(priceAtDate) AS minPrice,
    max(priceAtDate) AS maxPrice,
    DATE(Prices.createdAt) AS creationDate
FROM
    Prices JOIN Products ON Prices.ProductId=Products.id
GROUP BY
    creationDate,
    ProductId


SELECT min(priceAtDate) AS minPrice, max(priceAtDate) AS maxPrice, DATE(Prices.createdAt) AS creationDate FROM Prices WHERE ProductId= :id GROUP BY creationDate, ProductId    