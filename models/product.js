var models = require("../models");
var log4js = require("log4js");
var logger = log4js.getLogger("root");

module.exports = function(sequelize, DataTypes) {
  var Product = sequelize.define("Product", {
    productName:  DataTypes.STRING,
    description:  DataTypes.STRING,
    productSeller:       DataTypes.STRING,
    status:       DataTypes.ENUM('TRACKED', 'UNTRACKED'),
    currentPrice: DataTypes.DECIMAL(10,2),
    originalPrice: DataTypes.DECIMAL(10,2),     
    desiredPrice: DataTypes.DECIMAL(10,2),     
    dateRemoved:  DataTypes.DATE,
    dateAdded: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    productUrl: {
      type: DataTypes.STRING,
      validate: {
        isUrl: true    
      }
    }
  }, 
  { 
  	classMethods: {
    	associate: function(models) {
    		Product.hasMany(models.Price);      		
  	  }
    },
    instanceMethods: {
      getPriceRange: function(callback) {        
        sequelize.query("SELECT min(priceAtDate) AS minPrice, max(priceAtDate) as maxPrice FROM Prices where ProductId= :id", 
          {
            replacements: {
              id: this.id
            },
            type: sequelize.QueryTypes.SELECT
          })
        .then(function(priceFound) {          
          callback(priceFound[0]);           
        })
        .catch(function(error) {                  
          callback(null);
        });
      },
      getPriceHistory: function(callback) {
        var query = "SELECT min(priceAtDate) AS minPrice, max(priceAtDate) AS maxPrice, DATE(Prices.createdAt) AS creationDate FROM Prices WHERE ProductId= :id GROUP BY creationDate, ProductId";
        sequelize.query(query, 
          {
            replacements: {
              id: this.id
            },
            type: sequelize.QueryTypes.SELECT
          })
        .then(function(prices) {          
          callback(prices);           
        })
        .catch(function(error) {                  
          callback(null);
        });  
      }
    }
  } 
  );
  return Product;
};