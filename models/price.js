module.exports = function(sequelize, DataTypes) {
  var Price = sequelize.define("Price", {
    priceAtDate: DataTypes.DECIMAL(10,2),
    dateAdded: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    }
  });
  return Price;
};