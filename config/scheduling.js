var log4js = require("log4js");
var logger = log4js.getLogger("root");
var emailManager = require('../util/emailManager');
var async = require('async');
var CronJob = require('cron').CronJob;
var models  = require('../models');
var requireDir = require('require-dir'); 
var crawlerList = requireDir('../crawlers/');

var crawlers = {};  
var crawlerManager = {}
var trackedCrawlerJob = null;

crawlerManager.initializeCrawlers = function() {
  try {
    for (var crawlerName in crawlerList) {
      if (crawlerName != 'base') {
        var crawler = new crawlerList[crawlerName];
        crawlers[crawlerName] = crawler;
        logger.debug("Added crawler: " + crawler.crawlerName);
      }
    }
    logger.info("Crawlers initialized...");
  } 
  catch (e) {
    logger.error("Problem initializing crawlers!");
    logger.error(e);
  }
}

crawlerManager.startCrawling = function(config) {
  if (trackedCrawlerJob != null) {
    trackedCrawlerJob.stop();  
  }
  trackedCrawlerJob = new CronJob(config.crawlerSchedule, function() {
    // 1. Read schedule config
    // 2. Retrieve all products from db
    // 3. For each product:
    // 4. Scrape url using appropiate crawler
    // 5. Persist price for product in price table
    // 6. If price for product has hit targeted price, send email
    
    try {
      var dateNow = new Date();
      var crawlerStartTime = dateNow.getTime();
      logger.info("Crawler running at " + dateNow);
      async.waterfall([
      function(callback) {
        models.Product.findAll({
          where: {
            status: 'TRACKED'
          }
        }).then(function(trackedProducts) {  
            callback(null, trackedProducts);
        }).catch(function(error) {
          callback(error);
          logger.error(error);  
        });    
      },
      function(trackedProducts, callback) {
        async.each(trackedProducts, function(product, callback2) {
          var tempCrawler = crawlers[product.productSeller + "Crawler"];
          var url = product.productUrl;
          tempCrawler.scrape(url, tempCrawler, function(productInfo) {
            product.currentPrice = productInfo.price.replace(/[^\d.]/g, '');                          
            product.save().then(function() {
              product.addPrice(price);
              var price = models.Price.build({
                priceAtDate: product.currentPrice,
                ProductId: product.id
              });
              price.save().then(function() {
                if (config.sendEmails == true || config.sendEmails == 'true') {
                  if (product.currentPrice <= product.desiredPrice && product.desiredPrice > 0) {
                    emailManager.sendEmail(product);  
                  }    
                }
              });
            });            
          }); 
          logger.debug("--- Product: " + product.productName);
          callback2(null)

        }, function(err) {
            if (err) {
              logger.error(err);
              callback(err);
            }

            callback(null);
          }
        );
      }],
      function (err,result) {
        logger.info("Crawling elapsed time: " + (new Date().getTime() - crawlerStartTime) + "ms");  
      });  
    } catch(e) {
      logger.error(e);
    }
    
  }, onCrawlerJobStop, true, null);  
}

function onCrawlerJobStop() {
  logger.info("Previous crawler job has been stopped...");  
}

module.exports = crawlerManager;