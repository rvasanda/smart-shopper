var cheerio = require('cheerio');
var BaseCrawler = require("./baseCrawler");

function JYSKCrawler() {
	BaseCrawler.call(this,"jysk");		
};

JYSKCrawler.prototype = Object.create(BaseCrawler.prototype);

JYSKCrawler.prototype.constructor = JYSKCrawler;

JYSKCrawler.prototype.customScrape = function(body) {
	$ = cheerio.load(body);
	
	var productInfo = {};

	if ($(".regular-price .price-integer").text() != "") {
		productInfo.price = $(".regular-price .price-integer").text();	
	} else if ($(".reduced-price .price-integer").text() != "") {
		productInfo.price = $(".reduced-price .price-integer").text();	
	} else {
		productInfo.price = $(".original-price .price-integer").text();
	}
	productInfo.name = $(".product-name").text();

	return productInfo; 
};

module.exports = JYSKCrawler;