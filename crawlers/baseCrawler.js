var cheerio = require('cheerio');
var request = require('request');
var log4js = require("log4js");
var logger = log4js.getLogger("root");

var BaseCrawler = function(crawlerName) {
	this.crawlerName = crawlerName != undefined ? crawlerName : 'base';
	this.urls = [];
};

BaseCrawler.prototype.crawl = function() {
	logger.info(this.crawlerName + " crawler is crawling: " + new Date());
	for (var i = 0; i < this.urls.length; i++){
	 	this.scrape(this.urls[i],this);
	 }
	logger.info(this.crawlerName + " crawler is finished crawling: " + new Date());
};

BaseCrawler.prototype.scrape = function(url, inst, cb) {
	request({
    method: 'GET',
    url: url
	}, 
	function(err, response, body) {
    if (err) {
    	logger.error("Could not extract body from url:  " + url);
    	logger.error(err);
    	return console.error(err);
    }
		var productInfo = inst.customScrape(body); 
		cb(productInfo);
  });
}

BaseCrawler.prototype.customScrape = function(body) {
	logger.info(this.crawlerName + " crawler has nothing to crawl");
	return null;
};

BaseCrawler.prototype.preProcessUrl = function(url) {
	logger.info(this.crawlerName + " crawler has nothing to preprocess");
	return url;
};

module.exports = BaseCrawler;