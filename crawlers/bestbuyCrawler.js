var cheerio = require('cheerio');
var BaseCrawler = require("./baseCrawler");

function BestbuyCrawler() {
	BaseCrawler.call(this,"bestbuy");		
};

BestbuyCrawler.prototype = Object.create(BaseCrawler.prototype);

BestbuyCrawler.prototype.constructor = BestbuyCrawler;

BestbuyCrawler.prototype.customScrape = function(body) {
	$ = cheerio.load(body);
	
	var productInfo = {};
	productInfo.price = $(".price-extra-large .prodprice").text();
	productInfo.name = $(".product-title").text();

	return productInfo; 
};

module.exports = BestbuyCrawler;