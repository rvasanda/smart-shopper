var cheerio = require('cheerio');
var BaseCrawler = require("./baseCrawler");

function AmazonCrawler() {
	BaseCrawler.call(this,"amazon");		
};

AmazonCrawler.prototype = Object.create(BaseCrawler.prototype);

AmazonCrawler.prototype.constructor = AmazonCrawler;

AmazonCrawler.prototype.customScrape = function(body) {
	$ = cheerio.load(body);
	
	var productInfo = {};

	productInfo.price = $("#priceblock_ourprice").text();	
	productInfo.name = $("#productTitle").text();

	return productInfo; 
};

module.exports = AmazonCrawler;