var cheerio = require('cheerio');
var BaseCrawler = require("./baseCrawler");

function CanadaComputersCrawler() {
	BaseCrawler.call(this,"canadacomputers");		
};

CanadaComputersCrawler.prototype = Object.create(BaseCrawler.prototype);

CanadaComputersCrawler.prototype.constructor = CanadaComputersCrawler;

CanadaComputersCrawler.prototype.customScrape = function(body) {
	$ = cheerio.load(body);
	
	var productInfo = {};
	productInfo.price = $("#SalePrice").text();
	productInfo.name = $(".item_title h1").text();

	return productInfo; 
};

module.exports = CanadaComputersCrawler;