var cheerio = require('cheerio');
var BaseCrawler = require("./baseCrawler");

function NeweggCrawler() {
	BaseCrawler.call(this,"newegg");		
};

NeweggCrawler.prototype = Object.create(BaseCrawler.prototype);

NeweggCrawler.prototype.constructor = NeweggCrawler;

NeweggCrawler.prototype.customScrape = function(body) {
	$ = cheerio.load(body);
	var productInfo = {};
	// productInfo.price = $("#singleFinalPrice").attr("content");
	//productInfo.price = $("#TotalPrice").text();
	productInfo.price = $("li[content]").text();	
	productInfo.name = $("#grpDescrip_h").text();
	return productInfo; 
};

module.exports = NeweggCrawler;
