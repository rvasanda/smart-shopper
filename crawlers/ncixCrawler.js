var cheerio = require('cheerio');
var BaseCrawler = require("./baseCrawler");

function NCIXCrawler() {
	BaseCrawler.call(this,"ncix");		
};

NCIXCrawler.prototype = Object.create(BaseCrawler.prototype);

NCIXCrawler.prototype.constructor = NCIXCrawler;

NCIXCrawler.prototype.customScrape = function(body) {
	$ = cheerio.load(body);
	
	var productInfo = {};
	productInfo.price = $("#div_price td font[color='#CC0000'][size='4']").text();
	productInfo.name = $(".h1description").text();

	return productInfo; 
};

NCIXCrawler.prototype.preProcessUrl = function(url) {
	var start = url.lastIndexOf('-');
	var end = url.lastIndexOf('.');
	if (end - start == 5) {
		return url.substring(0,start) + url.substring(end);		
	}
	return url;
};

module.exports = NCIXCrawler;