$("#trackedProductsTable").on('click', "tbody tr .product-name", function() {
  var url = $(this).closest("tr").find(".product-link").attr("href");
  generateHistory(url);
  generateSummary(url);  
});


function generateHistory(url) {
  $.post("getProductPriceHistory",
      {
        productUrl: url
      },
      onPriceHistoryRetrieveSuccess
    );    
}

function onPriceHistoryRetrieveSuccess(data) {
  chartData = [];
  for (var i = 0; i < data.length; i++) {
    chartData[i] = {
      x: new Date(data[i].creationDate),
      y: data[i].minPrice 
    }
  }
  console.log(chartData);
  var chart = new CanvasJS.Chart("chartContainer",
  {
    theme: "theme2",
    animationEnabled: true,
    axisY: {
      title: "Price"
    },
    axisX: {
      title: "Month",
      valueFormatString: "MMM",
      interval:1,
      intervalType: "month"
    },
    data: [
    {
      type: "splineArea", //change it to line, area, bar, pie, etc
      dataPoints: chartData
    }
    ]
  });
  chart.render();
}

function generateSummary(url) {
  $.post("getProductPriceSummary",
      {
        productUrl: url
      },
      onPriceSummaryRetrieveSuccess
    );    
}

function onPriceSummaryRetrieveSuccess(trackedProduct) {
  if (trackedProduct.desiredPrice == null) {
    $(".summary .desiredPrice").text("N/A");
  } else {
    $(".summary .desiredPrice").text("$" + trackedProduct.desiredPrice.toFixed(2));
  }
  
  $(".summary .currentPrice").text("$" + trackedProduct.currentPrice.toFixed(2));
  $(".summary .lowPrice").text("$" + trackedProduct['minPrice'].toFixed(2));
  $(".summary .originalPrice").text("$" + trackedProduct.originalPrice.toFixed(2));
  $(".summary .highPrice").text("$" + trackedProduct['maxPrice'].toFixed(2));
  $(".summary").removeClass('ss-hidden');
}