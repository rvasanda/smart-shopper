$("#trackedProductsTable > tbody").on("mouseenter", "tr", function() {
	$(this).find("span").removeClass("ss-hidden");
});

$("#trackedProductsTable > tbody").on("mouseleave", "tr", function() {
	$(this).find("span").addClass("ss-hidden");
});

function recalculateTableIndexes() {
	$("#trackedProductsTable > tbody > tr").each(function(i) {
  	$this = $(this)
  	var indexColumn = $this.find("td:first");  
  	indexColumn.text(i+1);
	});
}

$("#trackedProductsTable").on('click', "tbody > tr", function() {
	$("#trackedProductsTable > tbody > tr").removeClass("bg-info")
	$(this).closest("tr").addClass("bg-info");	
});

$( document ).ready(function() {
	var fixHelperModified = function(e, tr) {         
		var $originals = tr.children();         
		var $helper = tr.clone();         
		$helper.children().each(function(index) {           
			$(this).width($originals.eq(index).width())         
		});         
		return $helper;     
	}; 
	$("#trackedProductsTable tbody").sortable({
		helper: fixHelperModified,
		stop: function(event,ui) {
			recalculateTableIndexes();
		}	
	}).disableSelection();
	
	$.get("getAllProducts",
  	function(allProducts) {
  		for (var i = 0; i < allProducts.length; i++) {
  			productAddSucess(allProducts[i]);	
  		}	
  	}
	);		   
});

function productAddSucess(trackedProduct) {
	if (trackedProduct) {
		var tableSize = $('#trackedProductsTable tbody tr').size();	
		var sampleProductRowHtml = $('.sampleProductRow').html();
		var productRow = document.createElement('tr');

		$(productRow).data('id',tableSize+1);	
		$(productRow).html(sampleProductRowHtml);
		$(productRow).find(".product-name").text(trackedProduct.productName);
		$(productRow).find(".product-link").attr("href", trackedProduct.productUrl);
		$(productRow).find(".product-link").text(trackedProduct.productSeller);
		$(productRow).find(".product-price").text("$" + trackedProduct.currentPrice.toFixed(2));

		$('#trackedProductsTable > tbody:last-child').append(productRow);
		recalculateTableIndexes();		
	}
}

$.fn.filterByData = function(prop, val) {
    return this.filter(
        function() { return $(this).data(prop)==val; }
    );
}