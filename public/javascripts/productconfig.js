$("#trackProductBtn").click(function(){
	var url = $(".productUrl").val();
	var host = parseURL(url).host;

	if (url && host) {
		$.post("trackProduct",
			{
				productUrl: url,
				productSeller: host  
			},
	  	productAddSucess
		);	
	}
});

$('#stopTrackingBtn').on("click", function() {
		var id = $("#deleteProductModal").data('id');		
		var url = $("#trackedProductsTable tbody tr").filterByData("id",id).find(".product-link").attr("href");
		$.post("untrackProduct",
			{
				productUrl: url
			},
	  	productRemoveSuccess
		);	
});

function productRemoveSuccess() {
	var id = $("#deleteProductModal").data('id');
	$('#trackedProductsTable tbody tr').filterByData('id',id).replaceWith("");
	recalculateTableIndexes();
	$("#deleteProductModal").modal('hide');	
}

$(".selectPriceValue").click(function(){
	$("#targetedPricePercent").addClass("hidden");
	$("#targetedPriceValue").removeClass("hidden");
});

$(".selectPricePercent").click(function(){
	$("#targetedPriceValue").addClass("hidden");
	$("#targetedPricePercent").removeClass("hidden");
});

$('#trackedProductsTable').on('click', '.editIcon', function(){
   	var id = $(this).closest("tr").data('id');   	
   	$("#targetedPriceValue :input").val("");
   	$("#targetedPricePercent :input").val("");
   	$("#productConfigModal").data('id',id).modal('show');
});

$('#trackedProductsTable').on('click', '.deleteIcon', function(){
   	var id = $(this).closest("tr").data('id');   	
   	$("#deleteProductModal").data('id',id).modal('show');
});

function setDesiredPriceSuccess() {
	$("#productConfigModal").modal('hide');	
}

$("#saveProductConfigBtn").on('click', function() {
	var id = $("#productConfigModal").data('id');		
	var url = $("#trackedProductsTable tbody tr").filterByData("id",id).find(".product-link").attr("href");
	var desiredPrice = 0.00;

	if ($("#targetedPriceValue").hasClass("hidden") == false) {
		desiredPrice = $("#targetedPriceValue :input").val();
	} else if ($("#targetedPricePercent").hasClass("hidden") == false) {
		var percent = Number($("#targetedPricePercent :input").val());
		var currentPrice = $("#trackedProductsTable tbody tr").filterByData("id",id).find(".product-price").text().replace("$","");
		desiredPrice = (parseFloat(currentPrice) * ( (100 - percent) / 100)).toFixed(2); 
	} else {
		console.log("No product config changes made");
		return;	
	}
	$.post("setDesiredPrice",
		{
			desiredPrice: desiredPrice,
			productUrl: url
		},
		setDesiredPriceSuccess  	
	);	
});