$(".glyphicon-wrench").click(function() {
	$("#globalConfigModal").modal('show');	
});

$("#enableNotificationsBtn").click(function() {
	$("#disableNotificationsBtn").removeClass("btn-primary")
	$("#enableNotificationsBtn").addClass("btn-primary");
});

$("#disableNotificationsBtn").click(function() {
	$("#enableNotificationsBtn").removeClass("btn-primary")
	$("#disableNotificationsBtn").addClass("btn-primary");
});

$("#saveGlobalConfigBtn").click(function() {
	var cronSchedule = $("#crawlerSchedule").val();
	var sendEmails = false;

	if ($("#enableNotificationsBtn").hasClass("btn-primary")) {
		sendEmails = true;
	}

	$.post("savePreferences",
		{
			crawlerSchedule: cronSchedule,
			sendEmails: sendEmails		
		},
		onPreferencesSaveSuccess
	);					
});

function onPreferencesSaveSuccess() {
	$("#globalConfigModal").modal('hide');
}