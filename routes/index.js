var express = require('express');
var cheerio = require('cheerio');
var request = require('request');
var requireDir = require('require-dir');
var log4js = require("log4js");
var logger = log4js.getLogger("root");
var router = express.Router();

var models  = require('../models');
var config = require("../config/config.json");
var crawlerList = requireDir('../crawlers/');
var crawlerManager = require("../config/scheduling");

//Request param declarations

var NAME 			         = 'productName';
var SELLER             = 'productSeller';
var URL				         = 'productUrl';
var CURRENT_PRICE      = 'currentPrice';
var DESIRED_PRICE      = 'desiredPrice';
var SEND_EMAILS        = 'sendEmails';
var CRAWLER_SCHEDULE   = 'crawlerSchedule';

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('main', { title: 'Express' });
});

router.post('/trackProduct', function(req, res, next) {    
  // var name = req.param(NAME) ? req.param(NAME) : "";
  var url = req.body[URL] ? req.body[URL] : "";
  var seller = req.body[SELLER] ? req.body[SELLER] : "";

  if (url) {
    // check if we are already tracking this product
    models.Product.findOne({
      where: {
        productUrl: url,
        status: 'TRACKED'
      }
    }).then(function(foundProduct) {
      if (foundProduct) {
        logger.debug("Already tracking product: " + foundProduct.productName);
        res.send(null);
      } else {
        
        // Need name and price
          try {
            var crawler = new crawlerList[seller + "Crawler"]; 
            var processedUrl = crawler.preProcessUrl(url);

            request({
              method: 'GET',
              url: url
            }, 
            function(err, response, body) {
              if (err) {
                 res.status(500).send({ error: err });
                // return console.error(err);
              }
              var productInfo = crawler.customScrape(body);

              productInfo.price = Number((productInfo.price).replace(/[^0-9\.]+/g,""));                

              models.Product
                .create({
                  productName: productInfo.name.trim(),
                  productSeller: seller.replace(" ", ""),
                  productUrl: processedUrl,
                  currentPrice: productInfo.price,
                  originalPrice: productInfo.price,
                  status: 'TRACKED' 
                })
                .then(function(trackedProduct) {          
                  logger.info("Product created: " + trackedProduct);
                  res.send(trackedProduct);
                }).catch(function(error) {        
                    logger.error("Product could not be created!");
                    logger.error(e);
                    res.status(500).render('error', { error: 'Product could not be tracked'});
                });

            });
          } catch(e) {
              logger.error(e);
              res.status(500).render('error', { error: 'Could not connect to vendor site from url: ' + url});
          }
      }      
    });
  }  
});

router.post('/untrackProduct', function(req, res, next) { 
  var url = req.body[URL] ? req.body[URL] : "";
  if (url) {
    models.Product.findOne({
      where: {
        productUrl: url
      }
    }).then(function(foundProduct) {
      if (foundProduct) {
        foundProduct.status='UNTRACKED';
        foundProduct.save();
        logger.info("Untracked product: " + foundProduct.productName);
        res.send("Untracked product: " + foundProduct.productName);
      } else {    
        res.status(500).render('error', { error: 'Could not find or untrack product based on url: ' + url});
      }
    });  
  }
});   

router.get('/getAllProducts', function(req, res, next) {
  models.Product.findAll({
      where: {
        status: 'TRACKED'
      }
  }).then(function(trackedProducts) {  
      logger.info("Retrieved " + trackedProducts.length + " products");
      res.send(trackedProducts);              
  }).catch(function(error) {
    logger.error(error);  
    res.status(500).render('error', { error: 'Could not retrieve list of products'});
  });
});

router.post('/setDesiredPrice', function(req, res, next) {
  var url = req.body[URL] ? req.body[URL] : "";
  var desiredPrice = req.body[DESIRED_PRICE] ? req.body[DESIRED_PRICE] : "";
  models.Product.findOne({
      where: {
        status: 'TRACKED',
        productUrl: url
      }
  }).then(function(trackedProduct) {  
      trackedProduct.desiredPrice = desiredPrice;
      trackedProduct.save();
      logger.info("Setting desired product price: " + desiredPrice);
      res.send(trackedProduct);              
  }).catch(function(error) {
    logger.error(error);  
    res.status(500).render('error', { error: 'Could not set price for product:' + trackedProduct.productName});
  });
});

router.post('/savePreferences', function(req, res, next) {
  var sendEmails = req.body[SEND_EMAILS] ? req.body[SEND_EMAILS] : true;
  var crawlerScheduleString = req.body[CRAWLER_SCHEDULE] ? req.body[CRAWLER_SCHEDULE] : "";
  try {
    if (sendEmails != config.sendEmails) {
      config.sendEmails = sendEmails;
      logger.info("Send email notifications set to: " + config.sendEmails);
    }
    if (crawlerScheduleString != "" && crawlerScheduleString != config.crawlerSchedule) {
      config.crawlerSchedule = crawlerScheduleString;  
      logger.info("Crawler schedule set to: " + config.crawlerSchedule);
      crawlerManager.startCrawling(config);
    }  
    res.send("Preferences Saved");
  } catch (e) {
    logger.error("Error setting config ");
    logger.error(e);  
    res.status(500).render('error', { error: 'Could not save global preferences' });
  }
});

router.post('/getProductPriceSummary', function(req, res, next) {
  var url = req.body[URL] ? req.body[URL] : null;
  if (url) {  
    models.Product.findOne({
        where: {
          status: 'TRACKED',
          productUrl: url
        }
    }).then(function(trackedProduct) {  
        logger.info("Retrieving product summary: " + trackedProduct.productName);
        
        trackedProduct.getPriceRange(function(priceInfo) {
          if (priceInfo) {
            logger.debug("-- Min Price: " + priceInfo.minPrice);
            logger.debug("-- Max Price: " + priceInfo.maxPrice);
            priceInfo.desiredPrice = trackedProduct.desiredPrice;
            priceInfo.currentPrice = trackedProduct.currentPrice;
            priceInfo.originalPrice = trackedProduct.originalPrice;
            res.send(priceInfo);    
          } else {
            logger.error("Could not get price range for product: " + trackedProduct);  
            res.status(500).render('error', { error: 'Could not get price range for product: ' + trackedProduct });
          }
        });                  
    }).catch(function(error) {
      logger.error("Could not find product by url: " + url);
      logger.error(error);
      res.status(500).render('error', { error: 'Could not find product by url: ' + url});
    });  
  } else {
    res.status(500).render('error', { error: 'Product url is not defined: ' + url});
  }
});

router.post('/getProductPriceHistory', function(req, res, next) {
  var url = req.body[URL] ? req.body[URL] : null;
  if (url) {  
    models.Product.findOne({
        where: {
          status: 'TRACKED',
          productUrl: url
        }
    }).then(function(trackedProduct) {  
        logger.info("Retrieving product history: " + trackedProduct.productName);
        trackedProduct.getPriceHistory(function(prices) {
          if (prices) {
            res.send(prices);    
          } else {
            logger.error("Could not get price history for product: " + trackedProduct);  
            res.status(500).render('error', { error: 'Could not get price history for product: ' + trackedProduct });
          }
        });                  
    }).catch(function(error) {
      logger.error("Could not find product by url: " + url);
      logger.error(error);
      res.status(500).render('error', { error: 'Could not find product by url: ' + url});
    });  
  } else {
    res.status(500).render('error', { error: 'Product url is not defined: ' + url});
  }
});

module.exports = router;