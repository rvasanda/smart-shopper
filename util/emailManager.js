var log4js = require("log4js");
var logger = log4js.getLogger("root");
var nodemailer = require("nodemailer");
var emailConfig = require("../config/config.json").emailConfig;

var emailManager = {};

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    service: emailConfig.service,
    auth: {
        user: emailConfig.user,
        pass: emailConfig.pass
    }
});

emailManager.sendEmail = function(trackedProduct) {
    
    var message = '<h2>' + trackedProduct.productName + '</h2>'
                  + '<br>' 
                  + '<p>The price for this product from seller ' + trackedProduct.productSeller + ' has been reduced from $<strong>' + trackedProduct.originalPrice + '</strong> to $<strong>' + trackedProduct.currentPrice + '</strong>. Act now!<p>'
                  + '<br>'
                  + '<p>Here is the link to the product page: ' + trackedProduct.productUrl + '<p>'
                  + '<br><br>'
                  + 'Regards,'
                  + '<br>'
                  + 'SmartShopper' 

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'SmartShopper <' + emailConfig.user + '>', // sender address
        to: 'Pieman, ' + emailConfig.user, // list of receivers
        subject: 'SmartShopper', // Subject line
        text: '', 
        html: message
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            logger.error("Could not send email");
            logger.error(error);
            return console.log(error);
        }
        logger.info("Email sent to: " + emailConfig.user);
        logger.info("--Product referenced in email: " + trackedProduct.productName);
    });
};

module.exports = emailManager;